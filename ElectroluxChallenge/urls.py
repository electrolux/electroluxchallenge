from django.urls import re_path, path

from ElectroluxChallenge import views

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    path('Categories/', views.categoriesList, name='categories'),
    re_path(r'^items/$', views.categoriesItems, name='items'),
    re_path(r'^itemsDetails/$', views.ItemsDetails, name='itemsDetails'),


]