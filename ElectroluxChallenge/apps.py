from django.apps import AppConfig


class ElectroluxchallengeConfig(AppConfig):
    name = 'ElectroluxChallenge'
