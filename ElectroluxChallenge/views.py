import requests
from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'list.html')
def categoriesList(request):
    categories=[]
    response = requests.get('https://pokeapi.co/api/v2/item-category/')
    products = response.json()
    for i in products['results']:
        categories.append(i)

    request.session['categories'] = categories

    return render(request, 'list.html',{'categories':categories})

def categoriesItems(request):

    list=[]
    categorieName=request.GET['name'];
    print("categorieName"+categorieName)
    response = requests.get('https://pokeapi.co/api/v2/item-category/'+categorieName)
    items = response.json()
    for j in items['items']:
        print(j['name'])
        response1 = requests.get('https://pokeapi.co/api/v2/item/' + j['name'])
        products = response1.json()
        j['sprites']=products['sprites'];
        j['cost']=products['cost']
        j['effect']=products['effect_entries']
        list.append(j)

    return render(request, 'list.html', {'name': categorieName, 'products': list,'categories':request.session['categories']})
def categoriesItems(request):

    list=[]
    categorieName=request.GET['name'];
    print("categorieName"+categorieName)
    response = requests.get('https://pokeapi.co/api/v2/item-category/'+categorieName)
    items = response.json()
    for j in items['items']:
        print(j['name'])
        response1 = requests.get('https://pokeapi.co/api/v2/item/' + j['name'])
        products = response1.json()
        j['sprites']=products['sprites'];
        j['cost']=products['cost']
        j['effect']=products['effect_entries']
        list.append(j)

    return render(request, 'list.html', {'name': categorieName, 'products': list,'categories':request.session['categories']})
def ItemsDetails(request):
    list=[]
    itemName=request.GET['itemName'];
    print("itemName"+itemName)
    response = requests.get('https://pokeapi.co/api/v2/item/'+itemName)
    items = response.json()

    return render(request, 'product_detail.html', {'itemName': itemName, 'cost': items['cost'],'image' : items['sprites'],'description':items['effect_entries']})

